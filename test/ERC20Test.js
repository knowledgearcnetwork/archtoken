const { BN, constants, expectEvent, shouldFail } = require('openzeppelin-test-helpers');
const { ZERO_ADDRESS } = constants;

const Token = artifacts.require("./ARCH");

const decimals = new BN('18');
const initialSupply = new BN('1025000000').mul(new BN('10').pow(decimals));
const tokenPerAccount = new BN('1000000000000000000000');

const MAX_UINT = new BN('115792089237316195423570985008687907853269984665640564039457584007913129639935');

contract('Token', async(accounts) => {
  const owner = accounts[0];
  const user1 = accounts[1];
  const user2 = accounts[2];
  const tokenHolders = [user1, user2];

  let token;

  it('Deploy Token', async() => {
    token = await Token.new();
  });

  it("Spread tokens to users", async() => {
    for (var i = 0; i < tokenHolders.length; i++) {
      await token.transfer(tokenHolders[i], tokenPerAccount);
      (await token.balanceOf(tokenHolders[i])).should.be.bignumber.equal(tokenPerAccount);
      // expect(await token.balanceOf(tokenHolders[i])).should.be.bignumber.equal(tokenPerAccount);
    }
    // // Check token ledger is correct
    let totalTokensCirculating = new BN('2').mul(tokenPerAccount);
    let remainingTokens = initialSupply.sub(totalTokensCirculating);
    console.log("remaining tokens: ", remainingTokens);
    console.log("owner balance: ", await token.balanceOf(owner));
    (await token.balanceOf(owner)).should.be.bignumber.equal(remainingTokens);
  });

  it('Fail to send ether to token contract', async() => {
    let err;
    try{
      await web3.eth.sendTransaction({from:user1, to: token.address, value: 10000})
    } catch(e){
      err = e;
    }
    assert.notEqual(err, undefined);
  });

  it('Fail to transfer to token contract', async() => {
    let err;
    try{
      await token.transfer(token.address, new BN('1000'));
    } catch(e){
      err = e;
    }
    assert.notEqual(err, undefined);
  });

  it('Fail to transfer to ZERO_ADDRESS', async() => {
    let err;
    try{
      await token.transfer(ZERO_ADDRESS, new BN('1000'));
    } catch(e){
      err = e;
    }
    assert.notEqual(err, undefined);
  });

  it('Fail to transfer from to token address', async() => {
    let err;
    await token.approve(token.address, new BN('1000'), {from: user1});
    try{
      await token.transferFrom(user1, token.address, new BN('1000'));
    } catch(e){
      err = e;
    }
    assert.notEqual(err, undefined);
  });

  it('Fail to transfer from to ZERO_ADDRESS', async() => {
    let err;
    try{
      await token.transferFrom(user1, ZERO_ADDRESS, new BN('1000'));
    } catch(e){
      err = e;
    }
    assert.notEqual(err, undefined);
  });

  it('check totalSupply', async() => {
    (await token.totalSupply()).should.be.bignumber.equal(initialSupply);
  });

  it('Check name', async() => {
      assert.equal(await token.name(), 'Archive');
  });

  it('Check symbol', async() => {
      assert.equal(await token.symbol(), 'ARCH');
  });

  it('Check decimals', async() => {
      (await token.decimals()).should.be.bignumber.equal('18');
  });

  it('Approve user', async() => {
    await token.approve(user1, new BN('1000'), {from: user2});
    (await token.allowance(user2, user1)).should.be.bignumber.equal(new BN('1000'));
  });

  it('Transfer From and verify allowance decreased', async() => {
    await token.transferFrom(user2, user1, new BN('1000'), {from: user1});
    (await token.allowance(user2, user1)).should.be.bignumber.equal('0');
  });

  it('Increase and Decrease allowance', async() => {
    await token.increaseAllowance(user1, new BN('1'), {from: user2});
    (await token.allowance(user2, user1)).should.be.bignumber.equal(new BN('1'));
    await token.decreaseAllowance(user1, new BN('1'), {from: user2});
    (await token.allowance(user2, user1)).should.be.bignumber.equal('0');
  });

  it("Approve max uint", async() => {
      await token.approve(user1, MAX_UINT, {from: owner});
      (await token.allowance(owner, user1)).should.be.bignumber.equal(MAX_UINT);
  });

  it('Fail to decrese allowance past 0', async() => {
      let err;
      await token.approve(user1, new BN('1'), {from: user2});
      try {
        await token.decreaseAllowance(user1, new BN('2'), {from: user2});
      } catch(e){
        err = e;
      }
      assert.notEqual(err, undefined);
  });

  it('Fail to increase allowance past 2^256', async() => {
      let err;
      await token.approve(user1, MAX_UINT, {from: user2});
      try {
        await token.increaseAllowance(user1, new BN('2'), {from: user2});
      } catch(e){
        err = e;
      }
      assert.notEqual(err, undefined);
  });

  it("Fail to mint tokens" , async() => {
      try {
        await token._mint(1000, owner);
      } catch(e){
        err = e;
      }
      assert.notEqual(err, undefined);
  });

  it("Fail to burn tokens" , async() => {
      try {
        await token._burn(1000, owner);
      } catch(e){
        err = e;
      }
      assert.notEqual(err, undefined);
  });


});
