## Prerequisites

- Truffle Framework
- Atom IDE
- Metamask

For testing:

- Solidity Coverage
- Mythril

## Deployment

### To Development

Truffle's Develop is used for testing although Ganache can be used by changing
the port number in the truffle.js file.

```
truffle develop
```

### Deploying to Ropsten TestNet

To deploy to the Ropsten testnet, create a .env file in your root directory.
Populate it with:

```
MNEMONIC=your twelve word mnemonic
ROPSTEN_URL=https://ropsten.infura.io/v3/ROPSTEN_API_KEY
```

Change both values to match your infura.io or local node settings.

### Deploying to Rinkeby TestNet

To deploy to the Rinkeby testnet, create a .env file in your root directory.
Populate it with:

```
MNEMONIC=your twelve word mnemonic
RINKEBY_URL=https://ropsten.infura.io/v3/RINKEBY_API_KEY
```

Change both values to match your infura.io or local node settings.

### Deploying to mainnet

To deploy to the Ethereum mainnet, create a .env file in your root directory.
Populate it with:

```
MNEMONIC=your twelve word mnemonic
MAINNET_URL=https://ropsten.infura.io/v3/MAINNET_API_KEY
```

Change both values to match your infura.io or local node settings.

IMPORTANT: Delete from .env immediately after deployment. There is no reason to
keep your mnemonic exposed even on your local machine.

### Flattening code for Etherscan.io

We use truffle flattener to flatten the contract for deployment to Etherscan.

To install:

```
npm install truffle-flattener
```

or, to install globally:

```
npm install truffle-flattener -g
```

To run:

```
truffle-flattener contracts/ARCH.sol > $(pwd)/build/contracts/ARCH.flattened.sol
```

## Testing & Auditing

Various unit tests have been written for testing the ARCH token. Most are based
on Open Zeppelin's ERC20 token testing suite. Chai is used for Javascript-based
unit tests.

The ARCH ERC20 token has been audited using the following tools:

### Truffle Develop

```
truffle develop
```

In another console run the tests:

```
truffle test
```

### Code Coverage

```
npm install --save-dev solidity-coverage
```

To run with the cloned project:

```
./node_modules/.bin/solidity-coverage
```

### Security Analysis

https://github.com/ConsenSys/mythril-classic/wiki

Once installed:

```
myth --truffle
```
